unit MainForm;

{
  Warren's Ping Program

}
interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  ICMPUnit,
  StdCtrls,
  ExtCtrls,
  Menus,
  ComCtrls,
  ImgList,
  IniFiles,
  Buttons,
  MPlayer,
  PingThread,
  ShellApi,
  ToolWin,
  System.ImageList;

type
  TPingForm = class(TForm)
    StatusMemo: TMemo;
    Timer1: TTimer;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    ImageList1: TImageList;
    ListView1: TListView;
    Panel2: TPanel;
    Label3: TLabel;
    ClearHostList1: TMenuItem;
    N1: TMenuItem;
    ToolBar1: TPanel;
    OptBtn: TSpeedButton;
    GoBtn: TSpeedButton;
    StopBtn: TSpeedButton;
    AddBtn: TSpeedButton;
    DelBtn: TSpeedButton;
    Splitter1: TSplitter;
    Panel3: TPanel;
    SpeedButton1: TSpeedButton;
    MediaPlayer1: TMediaPlayer;
    StartPinging1: TMenuItem;
    StopPinging1: TMenuItem;
    icmp1: Ticmp;
    procedure singleClick(Sender: TObject);
    procedure MultipleClick(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure TimedPing(Sender: TObject);
    procedure DoPing;
    procedure PingFormCreate(Sender: TObject);
    procedure PingingMode;
    procedure UserinputMode;
    procedure ExitClick(Sender: TObject);
    procedure AboutClick(Sender: TObject);
    procedure GoStopButtonHandler(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ListView1Edited(Sender: TObject; Item: TListItem; var S: String);
    procedure EditWavDblClick(Sender: TObject);
    procedure ClearHostList1Click(Sender: TObject);
    procedure EditProgDblClick(Sender: TObject);
    procedure OptBtnClick(Sender: TObject);
    procedure AddBtnClick(Sender: TObject);
    procedure DelBtnClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ListView1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure StartPinging1Click(Sender: TObject);
    procedure StopPinging1Click(Sender: TObject);
  private
    { Private declarations }
    FThread: TPingThread;
    FItem: TListItem;
    FPendingClose: Boolean; // Close when ping completes?
    FFailCount: Integer;
    FOldCaption: String;

    procedure LoadFromIni;

    procedure SaveToIni;

  public
    { Public declarations }
    function AddSite(sitename: String): TListItem;

    procedure PlaySound;
    procedure PingCompleted;
    procedure ExecuteErrorHandlingProgram(ErrorMsg: String);

  end;

var
  PingForm: TPingForm;
  IterationCount: Integer;

implementation

uses
  AboutFm,
  OptionsFm;

{$R *.DFM}

procedure TPingForm.DoPing;
var
  c, tm: Integer;
begin
  if FItem = NIL then
    exit;

  if FThread.Active then
    exit;

  tm := StrToIntDef(OptionsForm.TimeOutEdit.Text, 5);
  if (tm < 5) then
    tm := 5;
  if (tm > 10) then
    tm := 10;

  icmp1.TimeOut := (tm - 1) * 1000;
  icmp1.HostName := FItem.Caption;
  FThread.Active := true;

end;

procedure TPingForm.PingCompleted;
var
  c: Integer;
  ErrorMsg: String;
begin
  if FItem = NIL then
    exit;

  if (NOT FThread.PingOk) then
  begin
    ErrorMsg := 'Ping Error';
    FItem.StateIndex := FItem.StateIndex + 1;

    if icmp1.status = CICMP_RESOLVE_ERROR then
    begin

      FItem.SubItems[1] := 'Bad host name or DNS Unreachable';
      ErrorMsg := 'Error: Unable to resolve HostName ' + FItem.Caption;
      StatusMemo.Lines.Add(ErrorMsg);

    end
    else if icmp1.status = CICMP_NO_RESPONSE then
    begin
      FItem.SubItems[0] := icmp1.HostIP;
      FItem.SubItems[1] := 'No Response';
      ErrorMsg := icmp1.HostName + ' (' + icmp1.HostIP + ') No Response';
      StatusMemo.Lines.Add(ErrorMsg);
    end
    else
    begin
      ErrorMsg := 'Unknown error from TICMP: ' + inttostr(icmp1.status);
      FItem.SubItems[1] := ErrorMsg;
      StatusMemo.Lines.Add(ErrorMsg);
      IterationCount := 0; // Cancel any repeat Pings
    end;
    // increase FAIL count:
    c := StrToIntDef(FItem.SubItems[3], 0);
    Inc(c);
    FItem.SubItems[3] := inttostr(c);
    FItem.ImageIndex := 0;

    // If configured, play a sound:
    PlaySound;
    if OptionsForm.CheckBoxPopUp.Checked then
    begin
      Application.Restore;
      Application.BringToFront;
      Show;
    end;

    if (FItem.StateIndex = FFailCount) AND (FFailCount > 0) AND
      (Length(OptionsForm.EditProg.Text) > 0) then
    begin
      ExecuteErrorHandlingProgram(ErrorMsg);
    end;

  end
  else
  begin
    FItem.StateIndex := 0;

    StatusMemo.Lines.Add(icmp1.HostName + ' (' + icmp1.HostIP + ') Time:' +
      inttostr(icmp1.pIPEchoReply^.RTT) + ' ms');
    FItem.ImageIndex := 1;
    FItem.SubItems[0] := icmp1.HostIP;
    FItem.SubItems[1] := 'Ok. Time = ' +
      inttostr(icmp1.pIPEchoReply^.RTT) + ' ms';

    // increase OK count:
    c := StrToIntDef(FItem.SubItems[2], 0);
    Inc(c);
    FItem.SubItems[2] := inttostr(c);

  end;

  // Move to next item in list if there is one, else
  // back to the top:
  if (FItem.Index < ListView1.Items.Count) then
    FItem := ListView1.Items[FItem.Index + 1]
  else
    FItem := ListView1.Items[0];

  if FPendingClose then
  begin
    FThread.Active := false;
    Close;
  end;
end;

{ procedure TPingForm.PingClick(Sender: TObject);
  var
  tm:Integer;
  begin
  tm := StrToIntDef(TimeoutEdit.Text,5);
  if (tm<5) then
  tm := 5;

  PingingMode;
  Timer1.Enabled := False;
  icmp1.TimeOut := (tm-1) * 1000;
  icmp1.HostName := HostNameEdit.Text;
  if SingleRadio.checked = True then begin
  DoPing;
  UserinputMode;
  end else begin
  // Start Timer routine
  Timer1.Interval :=  tm * 1000;
  if Timer1.Interval < 1000 then Timer1.Interval := 1000;
  IterationCount :=  1;
  if IterationCount>=0 then begin
  DoPing;
  Timer1.Enabled := True;
  end;
  if Timer1.Enabled = False then UserinputMode;
  end;
  end; }

procedure TPingForm.UserinputMode;
begin
  Timer1.Enabled := false;
  // OptionsForm.HostNameEdit.Enabled := True;
  { PingButton.Enabled   := True;
    RadioGroup1.Enabled  := True;
    TimeOutEdit.Enabled  := True;
    TimeoutLabel.Enabled := True; }

  { SingleRadio.Enabled     := True;
    MultipleRadio.Enabled   := True;
    if SingleRadio.checked = True then begin
    PingCountEdit.Enabled   := False;
    TimeBetweenEdit.Enabled := False;
    PingCountLabel.Enabled  := False;
    IntervalLabel.Enabled   := False;
    end else begin
    PingCountEdit.Enabled   := True;
    TimeBetweenEdit.Enabled := True;
    PingCountLabel.Enabled  := True;
    IntervalLabel.Enabled   := True;
    end;

    CancelButton.Enabled    := False; }
end;

procedure TPingForm.PingingMode;
begin
  { HostNameEdit.Enabled := False;
    PingButton.Enabled   := False;
    RadioGroup1.Enabled  := False;
    TimeOutEdit.Enabled  := False;
    TimeoutLabel.Enabled := False;

    SingleRadio.Enabled     := False;
    MultipleRadio.Enabled   := False;
    PingCountEdit.Enabled   := False;
    TimeBetweenEdit.Enabled := False;
    PingCountLabel.Enabled  := False;
    IntervalLabel.Enabled   := False;

    CancelButton.Enabled    := True; }
end;

procedure TPingForm.singleClick(Sender: TObject);
begin
  { SingleRadio.Checked := True;
    MultipleRadio.Checked := False;
    PingCountEdit.Enabled := False;
    TimeBetweenEdit.Enabled := False;
    PingCountLabel.Enabled := False;
    IntervalLabel.Enabled := False; }
end;

procedure TPingForm.MultipleClick(Sender: TObject);
begin
  { SingleRadio.Checked := False;
    MultipleRadio.Checked := True;
    PingCountEdit.Enabled := True;
    TimeBetweenEdit.Enabled := True;
    PingCountLabel.Enabled := True;
    IntervalLabel.Enabled := True; }
end;

procedure TPingForm.CancelClick(Sender: TObject);
begin
  IterationCount := 0;
  Timer1.Enabled := false;
  UserinputMode;
end;

procedure TPingForm.TimedPing(Sender: TObject);
begin

  if FItem = NIL then
  begin
    if ListView1.Items.Count = 0 then
    begin
      GoBtn.Down := false;
      Timer1.Enabled := false;
      exit;
    end;
    FItem := ListView1.Items[0];
  end;


  // IterationCount := IterationCount - 1;
  // If IterationCount >= 0 then DoPing
  // else UserinputMode;

  DoPing;

end;

procedure TPingForm.PingFormCreate(Sender: TObject);
begin
  FOldCaption := Caption;
  FThread := TPingThread.Create(false); // run immediately.
  ListView1.Items.Clear;
end;

procedure TPingForm.ExitClick(Sender: TObject);
begin
  halt;
end;

procedure TPingForm.AboutClick(Sender: TObject);
begin
  AboutForm.ShowModal;
end;

function TPingForm.AddSite(sitename: String): TListItem;
var
  ListItem: TListItem;
begin
  ListItem := ListView1.Items.Add;
  ListItem.Caption := sitename;
  ListItem.ImageIndex := 2;
  ListItem.SubItems.Add('?');
  ListItem.SubItems.Add(' ');
  ListItem.SubItems.Add('0');
  ListItem.SubItems.Add('0');
  result := ListItem;
end;

procedure TPingForm.GoStopButtonHandler(Sender: TObject);
var
  t, tm: Integer;
begin

  Timer1.Interval := StrToIntDef(OptionsForm.TimeOutEdit.Text, 5) * 1000;

  FFailCount := StrToIntDef(OptionsForm.EditCount.Text, 0);

  if (GoBtn.Down) then
  begin
    Caption := FOldCaption + ' - Running';
    if ListView1.Items.Count = 0 then
    begin
      Application.MessageBox('No hosts given to ping!',
        'Add a host first!', MB_OK);
      Timer1.Enabled := false;
      GoBtn.Down := false;
      exit;
    end
    else
    begin
      FItem := ListView1.Items[0];
      for t := 0 to ListView1.Items.Count - 1 do
      begin
        ListView1.Items[t].StateIndex := 0;
        ListView1.Items[t].SubItems[2] := '0';
        ListView1.Items[t].SubItems[3] := '0';
      end;
    end;
  end
  else
  begin
    // Clear current item pointer when we stop:
    FItem := NIL;
    Caption := FOldCaption + ' - Stopped';

  end;

  tm := StrToIntDef(OptionsForm.TimeOutEdit.Text, 5);
  Timer1.Interval := tm * 1000;
  OptionsForm.TimeOutEdit.Text := inttostr(tm);

  Timer1.Enabled := GoBtn.Down;
  if Timer1.Enabled then
    TimedPing(Self);

  if GoBtn.Down then
  begin
    AddBtn.Enabled := false;
    DelBtn.Enabled := false;
  end
  else
  begin
    AddBtn.Enabled := true;
    DelBtn.Enabled := true;
  end;

end;

procedure TPingForm.SaveToIni;
var
  Ini: TIniFile;
  t: Integer;
  IniFileName: string;
begin

  IniFileName := ExtractFilePath(Application.ExeName) + '\PING.INI';
  Ini := TIniFile.Create(IniFileName);
  Ini.WriteInteger('settings', 'time',
    StrToIntDef(OptionsForm.TimeOutEdit.Text, 5));
  Ini.WriteBool('settings', 'active', GoBtn.Down);
  Ini.WriteBool('settings', 'popup', OptionsForm.CheckBoxPopUp.Checked);
  Ini.WriteString('settings', 'wavefile', OptionsForm.EditWav.Text);

  Ini.WriteString('fail', 'command', OptionsForm.EditProg.Text);
  Ini.WriteInteger('fail', 'failcount',
    StrToIntDef(OptionsForm.EditCount.Text, 0));

  if Ini.SectionExists('sites') then
  begin
    Ini.EraseSection('sites');
  end;
  for t := 0 to ListView1.Items.Count - 1 do
  begin
    Ini.WriteString('sites', 'site' + inttostr(t + 1),
      ListView1.Items[t].Caption);
  end;
  Ini.Destroy;
end;

procedure TPingForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  if FThread.Active then
  begin
    Action := caNone;
    Self.Caption := 'Shutting Down. One moment...';
    FPendingClose := true;
    exit;
  end;

  SaveToIni;

end;

procedure TPingForm.FormShow(Sender: TObject);
begin
  LoadFromIni;
end;

procedure TPingForm.LoadFromIni;
var
  Ini: TIniFile;
  t: Integer;
  sitename: String;
  IniFileName: string;
begin
  IniFileName := ExtractFilePath(Application.ExeName) + '\PING.INI';
  Ini := TIniFile.Create(IniFileName);

  if Ini.SectionExists('sites') then
  begin
    for t := 0 to 99 do
    begin
      sitename := Ini.ReadString('sites', 'site' + inttostr(t + 1), '');
      if (Length(sitename) = 0) then
        break;
      AddSite(sitename);
    end;

  end;

  Assert(Assigned(OptionsForm));

  OptionsForm.EditProg.Text := Ini.ReadString('fail', 'command', '');
  OptionsForm.EditCount.Text :=
    inttostr(Ini.ReadInteger('fail', 'failcount', 5));

  OptionsForm.TimeOutEdit.Text :=
    inttostr(Ini.ReadInteger('settings', 'time', 5));
  GoBtn.Down := Ini.ReadBool('settings', 'active', false);

  OptionsForm.EditWav.Text := Ini.ReadString('settings', 'wavefile', '');

  OptionsForm.CheckBoxPopUp.Checked := Ini.ReadBool('settings', 'popup', false);
  GoStopButtonHandler(Self);

  Ini.Destroy;
  ActiveControl := ListView1;
end;

procedure TPingForm.ListView1Edited(Sender: TObject; Item: TListItem;
  var S: String);
begin
  if Item.SubItems.Count >= 4 then
  begin
    Item.SubItems[2] := '0';
    Item.SubItems[3] := '0';
  end;
end;

procedure TPingForm.PlaySound;
begin
  if FileExists(OptionsForm.EditWav.Text) then
  begin
    try
      MediaPlayer1.FileName := OptionsForm.EditWav.Text;
      MediaPlayer1.Open;
      MediaPlayer1.Play;
    except
    end;
  end;
end;

procedure TPingForm.EditWavDblClick(Sender: TObject);
begin
  PlaySound;
end;

procedure TPingForm.ClearHostList1Click(Sender: TObject);
begin
  ListView1.Items.Clear;
end;

procedure TPingForm.ExecuteErrorHandlingProgram(ErrorMsg: String);
var
  msg: String;
  c: Integer;
begin
  msg := OptionsForm.EditProg.Text;

  // Plop in the error message at the indicated spot:
  c := Pos('$M', msg);
  if c > 0 then
  begin
    msg[c] := '%';
    msg[c + 1] := 's';
    msg := Format(msg, [ErrorMsg]);
  end;

  ShellExecute(Handle, 'open', 'cmd.exe', PChar('/c ' + msg), NIL, SW_HIDE);
end;

procedure TPingForm.EditProgDblClick(Sender: TObject);
begin
  ExecuteErrorHandlingProgram('Testing Error Handling Program');
end;

procedure TPingForm.OptBtnClick(Sender: TObject);
begin
  OptionsForm.Show;
end;

procedure TPingForm.AddBtnClick(Sender: TObject);
var
  NewItem: TListItem;
begin
  NewItem := AddSite('<New>');
  if NOT NewItem.EditCaption then
    NewItem.Destroy;
end;

procedure TPingForm.DelBtnClick(Sender: TObject);
begin
  if ListView1.Selected = NIL then
    exit;
  ListView1.Items.Delete(ListView1.Selected.Index);
  FItem := NIL;
end;

procedure TPingForm.SpeedButton1Click(Sender: TObject);
begin
  StatusMemo.Clear;
end;

procedure TPingForm.ListView1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // StatusMemo.Lines.Add( 'Key = '+IntToStr(Key) );
  if NOT ListView1.IsEditing then
  begin
    if (Key = 45) then
      AddBtnClick(Sender);
    if (Key = 46) then
      DelBtnClick(Sender);
  end;
end;

procedure TPingForm.StartPinging1Click(Sender: TObject);
begin
  GoBtn.Down := true;
  GoStopButtonHandler(Sender);
end;

procedure TPingForm.StopPinging1Click(Sender: TObject);
begin
  StopBtn.Down := true;
  GoStopButtonHandler(Sender);
end;

end.
