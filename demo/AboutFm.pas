unit AboutFm;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  ExtCtrls,
  StdCtrls,
  ICMPUnit;

type
  TAboutForm = class(TForm)
    OKButton: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Image1: TImage;
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutForm: TAboutForm;

implementation

{$R *.DFM}

procedure TAboutForm.OKClick(Sender: TObject);
begin
  ModalResult := 1;
end;

end.
