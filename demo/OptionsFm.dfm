�
 TOPTIONSFORM 0�  TPF0TOptionsFormOptionsFormLeft� Top� BorderStylebsDialogCaptionPing OptionsClientHeight� ClientWidth#Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel6LeftTopWidthHHeightCaptionUpdate Speed:  TLabelLabel4Left� TopWidth(HeightCaptionseconds  TEditTimeOutEditLeft`TopWidth!HeightTabOrder Text5  	TGroupBox	GroupBox1LeftTop,WidthHeightyCaptionOn ErrorTabOrder TLabelLabel1LeftTop(Width6HeightCaption
Play Sound  TSpeedButtonSpeedButton1Left� Top$WidthHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
:  6  BM6      v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ���������   �     ���   �3333��   ��33330��   ��3333�   ���33330�   ���        ���������   ���������   ���   ���   � ���� �   ������� �   ���������   ����� ��   ���������   ���������   
ParentFontOnClickSpeedButton1Click  TLabelLabel2LeftTopDWidthHeightCaptionAfter  TLabelLabel5LeftDTopDWidth� HeightCaption"failures in a row, execute command  TEditEditWavLeftHTop$Width� HeightTabOrder 
OnDblClickEditWavDblClick  	TCheckBoxCheckBoxPopUpLeftTopWidtheHeightCaptionPop Up WindowTabOrder  TEdit	EditCountLeft$Top@WidthHeightTabOrderText0  TEditEditProgLeftTopXWidth� HeightTabOrder   TButtonButton1LeftTop� WidthKHeightCaptionOkDefault	TabOrderOnClickButton1Click  TButtonButton2Left� Top� WidthKHeightCancel	CaptionCancelTabOrderOnClickButton2Click  TOpenDialogOpenDialog1
DefaultExt*.wavFilterSound Files|*.WAV|All Files|*.*TitleSelect a WAV File to PlayLeft� Top   