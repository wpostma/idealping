unit OptionsFm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TOptionsForm = class(TForm)
    Label6: TLabel;
    TimeOutEdit: TEdit;
    Label4: TLabel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    Label5: TLabel;
    EditWav: TEdit;
    CheckBoxPopUp: TCheckBox;
    EditCount: TEdit;
    EditProg: TEdit;
    Button1: TButton;
    Button2: TButton;
    OpenDialog1: TOpenDialog;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EditWavDblClick(Sender: TObject);
  private
    { Private declarations }
     FOldSpeed,FOldSound,FOldFailCount,FOldCmd:String;
     FOldPopup:Boolean;

  public
    { Public declarations }
  end;

var
  OptionsForm: TOptionsForm;

implementation

uses MainForm;

{$R *.DFM}

procedure TOptionsForm.SpeedButton1Click(Sender: TObject);
begin
 if OpenDialog1.Execute then
     EditWav.Text := OpenDialog1.Filename;
end;

procedure TOptionsForm.FormShow(Sender: TObject);
begin
     FOldSpeed  :=  TimeOutEdit.Text;
     FOldSound  :=  EditWav.Text;
     FOldFailCount:=  EditCount.Text;
     FOldCmd    := EditProg.Text;
     FOldPopup  :=  CheckBoxPopUp.Checked;
end;

procedure TOptionsForm.Button2Click(Sender: TObject);
begin
    TimeOutEdit.Text := FOldSpeed;
    EditWav.Text  :=  FOldSound;
    EditCount.Text :=  FOldFailCount;
    EditProg.Text  := FOldCmd;
    CheckBoxPopUp.Checked := FOldPopup;
    Close;
end;

procedure TOptionsForm.Button1Click(Sender: TObject);
begin
    Close;
end;

procedure TOptionsForm.FormCreate(Sender: TObject);
var
  buf:Array[0..128] of Char;
begin
  // A place with some sound files in it:
  GetWindowsDirectory(buf,128);
  OptionsForm.OpenDialog1.InitialDir := String(buf)+'\media';
end;

procedure TOptionsForm.EditWavDblClick(Sender: TObject);
begin
    PingForm.PlaySound;
end;

end.
