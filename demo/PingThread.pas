unit PingThread;

interface

uses
  Classes,Windows; 

type
  TPingThread = class(TThread)

  private
    { Private declarations }
    FActive     : Boolean;
    FPingResult : Pointer;
//  FModbusDriver : TZTRModbus;

  protected

   // constructor Create (aOwner : TComponent); override;
  //  procedure   SetZTRModbus( ModbusDriver : TZTRModbus );
    procedure   Execute; override;

    function GetPingOk:Boolean;
    
  public

    property Active : Boolean read FActive write FActive;
    property PingOk : Boolean read GetPingOk;
    
  end;

implementation

uses MainForm;

{ Important: Methods and properties of objects in VCL can only be used in a
  method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TAutoCommThread.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end;
}


function TPingThread.GetPingOk:Boolean;
begin
    result := FPingResult<>NIL;
end;

procedure TPingThread.Execute;
{var
   event : DWORD ;
   cstate : DWORD ;
   c      : Char ;}
begin
     while not Self.Terminated do begin
         if FActive then begin
            { Set active flag }
            { Do a Ping }
            FPingResult := PingForm.ICMP1.Ping;
            Synchronize(PingForm.PingCompleted);
            FActive := False;
         end else  { Not active }
         begin
            Sleep(50) ;
         end
    end;
end;

end.
