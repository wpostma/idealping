program Ping;

uses
  Forms,
  AboutFm in 'AboutFm.pas' {AboutForm},
  MainForm in 'MainForm.pas' {PingForm},
  OptionsFm in 'OptionsFm.pas' {OptionsForm},
  IcmpUnit in '..\IcmpUnit.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TPingForm, PingForm);
  Application.CreateForm(TAboutForm, AboutForm);
  Application.CreateForm(TOptionsForm, OptionsForm);
  Application.Run;
end.
